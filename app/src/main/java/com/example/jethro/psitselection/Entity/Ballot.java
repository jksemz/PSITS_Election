package com.example.jethro.psitselection.Entity;

/**
 * Created by JSP on 10/9/2017.
 */

public class Ballot {
    private String cand_id = "";
    private String rank_id = "";

    public Ballot(){
        // Empty Constructor
    }

    public Ballot(String cand_id, String rank_id){
        this.cand_id = cand_id;
        this.rank_id = rank_id;
    }

    public String getCand_id() {
        return cand_id;
    }

    public void setCand_id(String cand_id) {
        this.cand_id = cand_id;
    }

    public String getRank_id() {
        return rank_id;
    }

    public void setRank_id(String rank_id) {
        this.rank_id = rank_id;
    }
}

