package com.example.jethro.psitselection.Http;

import android.os.AsyncTask;

import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.Entity.Student;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Jet on 10/01/2017.
 */

public class GetStudent_ex extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... strings) {
        // Download JSON via HTTP Connection

        String JSONString = "";
        try {
            URL url = new URL(GlobalData.IP + "student.php");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            PrintStream ps = new PrintStream(conn.getOutputStream());
            ps.print("&id="+strings);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            JSONString = br.readLine();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONString;
    }

    @Override
    protected void onPostExecute(String s){
       try{
        JSONObject obj = new JSONObject(s);
           Student stud = new Student();
           stud.setId(obj.getString("ID"));
           stud.setFname(obj.getString("Firstname"));
           stud.setLname(obj.getString("Lastname"));
           stud.setVoted(obj.getBoolean("isVoted"));
       } catch (Exception e){
        // coming soon
       }
    }
}
