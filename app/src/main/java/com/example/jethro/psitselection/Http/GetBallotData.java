package com.example.jethro.psitselection.Http;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.jethro.psitselection.Activity.activity_01;
import com.example.jethro.psitselection.Activity.activity_02;
import com.example.jethro.psitselection.Entity.Candidate;
import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.Entity.Party;
import com.example.jethro.psitselection.Entity.Rank;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jet on 10/08/2017.
 */

public class GetBallotData extends AsyncTask<String, Void, String> {

    private boolean mode = true;
    public void setMode(boolean bool){
        mode = bool;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mode){
            activity_01.loading.setMessage("Loading Candidates");
            activity_01.loading.show();
        } else{
            // for act 2
            activity_02.loading.setMessage("Loading Candidates");
            activity_02.loading.show();
        }
    }

    @Override
    protected String doInBackground(String... string) {
        String JSONString = "";
        try {
            URL url = new URL(GlobalData.IP + "election/candidate.php");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            JSONString = br.readLine();

        } catch (Exception e) {
            Log.d("JSON!-BG", e.getMessage());
            Log.d("JSON!-BG", e.toString());
        }
        return JSONString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        ArrayList<Rank> rankList = new ArrayList<>();
        ArrayList<Party> partyList = new ArrayList<>();
        ArrayList<Candidate> candidateList = new ArrayList<>();

        try{
            JSONObject JSON = new JSONObject(s);

            //Parse Ranks ---------------------------------------------------------------------------------------------
            JSONArray rankJSONArray = JSON.getJSONArray("RANK");
            for(int x = 0; x < rankJSONArray.length(); x++){
                Rank lo_rank = new Rank(
                        rankJSONArray.getJSONObject(x).getString("rank_id"),
                        rankJSONArray.getJSONObject(x).getString("rank_name")
                );
                rankList.add(lo_rank);
            }

            //Parse Parties -------------------------------------------------------------------------------------------
            JSONArray partyJSONArray = JSON.getJSONArray("PARTY");
            for(int x = 0; x < partyJSONArray.length(); x++){
                Party lo_party = new Party();
                lo_party.setParty_id(partyJSONArray.getJSONObject(x).getString("party_id"));
                lo_party.setParty_name(partyJSONArray.getJSONObject(x).getString("party_name"));
                partyList.add(lo_party);
            }

            //Parse Candidates ----------------------------------------------------------------------------------------
            JSONObject candJSONObject = JSON.getJSONObject("CANDIDATE");
            for(int x = 0; x < rankJSONArray.length(); x++){
                JSONArray candJSONArray = candJSONObject.getJSONArray(rankList.get(x).getRankName());
                for(int y = 0; y < candJSONArray.length(); y++){
                    Candidate lo_cand = new Candidate();
                    lo_cand.setCand_id(candJSONArray.getJSONObject(y).getString("cand_id"));
                    lo_cand.setCand_name(candJSONArray.getJSONObject(y).getString("cand_name"));
                    lo_cand.setCand_votes(candJSONArray.getJSONObject(y).getInt("cand_votes"));
                    lo_cand.setRank_id(candJSONArray.getJSONObject(y).getString("rank_id"));
                    lo_cand.setParty_id(candJSONArray.getJSONObject(y).getString("party_id"));
                    candidateList.add(lo_cand);
                }
            }

            GlobalData.setRankList(rankList);
            GlobalData.setPartyList(partyList);
            GlobalData.setCandList(candidateList);

                GetImage imageTask = new GetImage();
                imageTask.setMode(mode);
                imageTask.execute("");
        } catch (Exception e){
            if(mode){
                activity_01.loading.dismiss();
            } else{
                // for act 3
                activity_02.loading.dismiss();
            }

            Log.d("JSON!-POST", e.getMessage());
            Log.d("JSON!-POST", e.toString());
            if(mode){
                AlertDialog.Builder builder = new AlertDialog.Builder(activity_01.getContext());
            } else{
                // for act 3
                AlertDialog.Builder builder = new AlertDialog.Builder(activity_02.getContext());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(activity_01.getContext());
            builder.setTitle("Reroute to Server");
            builder.setCancelable(false);
            final EditText et_ipaddr = new EditText(activity_01.getContext());
            et_ipaddr.setText(GlobalData.IP);
            et_ipaddr.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            ));

            builder.setView(et_ipaddr);
            builder.setNeutralButton("Set", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    GlobalData.IP = et_ipaddr.getText().toString();
                    execute("");
                }
            });

            Dialog ipDialog = builder.create();
            ipDialog.show();
        }
    }
}
