package com.example.jethro.psitselection.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jethro.psitselection.Entity.Ballot;
import com.example.jethro.psitselection.Entity.Candidate;
import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.Entity.Party;
import com.example.jethro.psitselection.Entity.Rank;
import com.example.jethro.psitselection.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jksem on 10/05/2017.
 */

public class BallotAdapter extends BaseExpandableListAdapter{

    private Context go_Context;
    private ArrayList<Candidate> candidateList;
    private ArrayList<Rank> rankList;
    private ArrayList<Party> partyList;
    // 2D Matrix Data Group by Rank
    private HashMap<String, ArrayList<Candidate>> candidateList2D;

    public BallotAdapter(Context iv_Context, ArrayList<Candidate> candidateList, ArrayList<Rank> rankList, ArrayList<Party> partyList){
        go_Context = iv_Context;
        this.candidateList = candidateList;
        this.rankList = rankList;
        this.partyList = partyList;
        candidateList2D = genHash(this.rankList, this.candidateList);
    }

    //transform header & child list into hashmap
    private HashMap<String, ArrayList<Candidate>> genHash(ArrayList<Rank> iv_ranks, ArrayList<Candidate> iv_cands){
        HashMap<String, ArrayList<Candidate>> hashMap = new HashMap<>();
        for(int x=0; x < iv_ranks.size(); x++){
            ArrayList<Candidate> lo_cands = new ArrayList<>();
            for(int y=0; y < iv_cands.size(); y++){
                if(iv_ranks.get(x).getRankId().equals(iv_cands.get(y).getRank_id()))
                    lo_cands.add(iv_cands.get(y));
            }
            hashMap.put(iv_ranks.get(x).getRankId(), lo_cands);
        }
        return hashMap;
    }

    //Selects 1 Candidate per Rank
    private void Select(String cand_id, String rank_id){
        ArrayList<Candidate> lo_cands = candidateList2D.get(rank_id);
        for(int x = 0; x < lo_cands.size(); x++){
            if(lo_cands.get(x).getCand_id().equals(cand_id))
                lo_cands.get(x).setVoted(true);
            else
                lo_cands.get(x).setVoted(false);
        }
        printVote(getBallotStash());
    }

    //Return Voted Candidates
    public ArrayList<Ballot> getBallotStash(){
        ArrayList<Ballot> ballotStash = new ArrayList<>();
        for(int x = 0; x < candidateList2D.size(); x++){
            ArrayList<Candidate> lo_cands = candidateList2D.get(rankList.get(x).getRankId());
            for(int y = 0; y < lo_cands.size(); y++){
                if(lo_cands.get(y).isVoted()){
                    Ballot lo_Ballot = new Ballot(lo_cands.get(y).getCand_id(), lo_cands.get(y).getRank_id());
                    ballotStash.add(lo_Ballot);
                }
            }
        }
        return ballotStash;
    }

    @Override
    public int getGroupCount() {
        return rankList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return candidateList2D.get(rankList.get(groupPosition).getRankId()).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return candidateList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return candidateList2D.get(rankList.get(groupPosition).getRankId()).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Rank lo_Rank = getRank(groupPosition);
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) go_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.cat_officer, null);
        }
        TextView tv_rank = (TextView) convertView.findViewById(R.id.tv_rank);
        tv_rank.setText(lo_Rank.getRankName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Candidate lo_candidate = getCand(groupPosition, childPosition);
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) go_Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.catitem_officer, null);
        }
        if(lo_candidate.getImg() != null) {
            ImageView img_face = (ImageView) convertView.findViewById(R.id.img_face);
            img_face.setImageBitmap(lo_candidate.getImg());
            img_face.setVisibility(View.VISIBLE);
        }
        //Log.d("BITMAP-SIZE (X,Y)", Integer.toString(lo_candidate.getImg().getWidth()) + "," + Integer.toString(lo_candidate.getImg().getHeight()));

        TextView tv_candname = (TextView) convertView.findViewById(R.id.tv_candname);
        tv_candname.setText(lo_candidate.getCand_name());

        TextView tv_party = (TextView) convertView.findViewById(R.id.tv_party);
        tv_party.setText(getPartyName(lo_candidate.getParty_id()));

        Button btn_vote = (Button) convertView.findViewById(R.id.btn_vote);
        if(lo_candidate.isVoted()){
            btn_vote.setText("voted");
        }
        else {
            btn_vote.setText("vote");
        }

        btn_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Select(lo_candidate.getCand_id(), lo_candidate.getRank_id());
                GlobalData.ballot = getBallotStash();
                notifyDataSetChanged();
            }
        });
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private Rank getRank(int groupPosition) {
        return rankList.get(groupPosition);
    }

    //Return partname using primary key
    private String getPartyName(String party_id){
        for(int x=0; x < partyList.size(); x++){
            Party lo_Party = partyList.get(x);
            if(lo_Party.getParty_id().equals(party_id))
                return lo_Party.getParty_name();
        }
        return "N/A";
    }

    private Candidate getCand(int groupPosition, int childPosition) {
        return candidateList2D.get(rankList.get(groupPosition).getRankId()).get(childPosition);
    }

    private void printVote(ArrayList<Ballot> list){
        Log.d("VOTES:"+list.size(), "----------------------------");
        for(int x=0; x < list.size(); x++){
            Log.d("CAND:", list.get(x).getCand_id());
            Log.d("RANK:", list.get(x).getRank_id());
        }
        Log.d("END", "--------------------------------");
    }
}
