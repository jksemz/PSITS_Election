package com.example.jethro.psitselection.Entity;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Jet on 10/08/2017.
 */

public class GlobalData {
    public static String ID = "";
    public static String PASS = "";
    public static String IP = "http://192.168.254.15:81/";
    public static Student student = null;
    public static ArrayList<Ballot> ballot = null;

    private static ArrayList<Rank> rankList = null;
    private static ArrayList<Candidate> candList = null;
    private static ArrayList<String> candKeyList = null;
    private static ArrayList<Party> partyList = null;

    public static ArrayList<Rank> getRankList() {
        return rankList;
    }

    public static void setRankList(ArrayList<Rank> rankList) {
        GlobalData.rankList = rankList;
    }

    public static ArrayList<Candidate> getCandList() {
        return candList;
    }

    public static void setCandList(ArrayList<Candidate> candList) {
        GlobalData.candList = candList;
        candKeyList = new ArrayList<>();
        for(int x = 0; x < candList.size() ;x++){
            candKeyList.add(candList.get(x).getCand_id());
        }
    }

    public static boolean isEmpty(){
        return rankList == null || candList == null;
    }

    public static ArrayList<Party> getPartyList() {
        return partyList;
    }

    public static void setPartyList(ArrayList<Party> partyList) {
        GlobalData.partyList = partyList;
    }

    public static ArrayList<String> getCandKeyList() {
        return candKeyList;
    }

    public static void setCandKeyList(ArrayList<String> candKeyList) {
        GlobalData.candKeyList = candKeyList;
    }

    public static void logout(){
        ID = "";
        PASS = "";
        student = null;
        ballot = null;
        for(int x=0; x < candList.size(); candList.get(x).setVoted(false), x++);
    }

    public static Bitmap getBitmap(String cand_id){
        for(int x = 0; x < candList.size(); x++){
            Candidate lo_cand = candList.get(x);
            if(cand_id.equals(lo_cand.getCand_id()))
                return lo_cand.getImg();
        }
        return null;
    }
}
