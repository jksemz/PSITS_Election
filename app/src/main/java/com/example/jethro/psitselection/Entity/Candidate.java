package com.example.jethro.psitselection.Entity;

import android.graphics.Bitmap;

/**
 * Created by jksem on 10/05/2017.
 */

public class Candidate {
    private String cand_id = "";
    private String cand_name = "";
    private int cand_votes = 0;
    private String rank_id = "";
    private String party_id = "";
    private boolean isVoted = false;
    private Bitmap img = null;

    public Candidate(){
        //
    }

    public String getCand_id() {
        return cand_id;
    }

    public void setCand_id(String cand_id) {
        this.cand_id = cand_id;
    }

    public String getCand_name() {
        return cand_name;
    }

    public void setCand_name(String cand_name) {
        this.cand_name = cand_name;
    }

    public int getCand_votes() {
        return cand_votes;
    }

    public void setCand_votes(int cand_votes) {
        this.cand_votes = cand_votes;
    }

    public String getRank_id() {
        return rank_id;
    }

    public void setRank_id(String rank_id) {
        this.rank_id = rank_id;
    }

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public boolean isVoted() {
        return isVoted;
    }

    public void setVoted(boolean voted) {
        isVoted = voted;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }


}
