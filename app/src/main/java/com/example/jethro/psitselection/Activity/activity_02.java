package com.example.jethro.psitselection.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.jethro.psitselection.Adapter.BallotAdapter;
import com.example.jethro.psitselection.Entity.Ballot;
import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.Http.GetBallotData;
import com.example.jethro.psitselection.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class activity_02 extends Activity implements View.OnClickListener{

    private static Context go_context;

    // Global Widget & Containers
    private TextView tv_id, tv_name;
    private Button btn_submit;
    private ExpandableListView el_ballot;
    private BallotAdapter ballot;

    public static ProgressDialog loading;
    protected Dialog msgDiag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_02);
        go_context = this;
        loading = new ProgressDialog(this);

        //Binding Widgets
        tv_id = (TextView) findViewById(R.id.tv_id);
        tv_name = (TextView) findViewById(R.id.tv_name);

        tv_name.setText(GlobalData.student.getFname());
        tv_id.setText(GlobalData.student.getId());

        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);

        el_ballot = (ExpandableListView) findViewById(R.id.el_ballot);
        ballot = new BallotAdapter(this, GlobalData.getCandList(), GlobalData.getRankList(), GlobalData.getPartyList());
        el_ballot.setAdapter(ballot);
    }

    @Override
    public void onClick(View v) {
        //code here
        switch (v.getId()){
            case R.id.btn_submit:
                if(GlobalData.ballot != null && isBallotComplete(GlobalData.ballot)){
                    // soon
                    new SendBallot().execute("");
                } else{
                    AlertDialog.Builder lo_builder = new AlertDialog.Builder(this);
                    lo_builder.setMessage("Please complete your votes!");
                    lo_builder.setNeutralButton("OK", null);
                    Dialog lo_dialog = lo_builder.create();
                    lo_builder.show();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        GlobalData.logout();
        this.finish();
    }

    public boolean isBallotComplete(ArrayList<Ballot> iv_ballotList){
        return iv_ballotList.size() == GlobalData.getRankList().size();
    }

    public static Context getContext(){
        return go_context;
    }

    public static void proceedTOResult(){
        Intent intent = new Intent(go_context ,activity_03.class);
        go_context.startActivity(intent);
    }

    class SendBallot extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setMessage("Sending Votes...");
            loading.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String output = "";
            try {
                JSONObject master = new JSONObject();
                JSONArray ballot = new JSONArray();

                ArrayList<Ballot> tempBallot = GlobalData.ballot;
                for (int x = 0; x < tempBallot.size(); ballot.put(x, tempBallot.get(x).getCand_id()), x++);
                master.put("ID", GlobalData.student.getId());
                master.put("BALLOT", ballot);
                Log.d("JSON PARSE", master.toString());
                String jsonString = master.toString();

                URL url = new URL(GlobalData.IP + "election/ballot.php");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", "application/json");

                conn.connect();

                OutputStream os = new BufferedOutputStream(conn.getOutputStream());
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(jsonString);
                writer.close();
                os.close();

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                output = br.readLine();


            } catch(Exception e){
                Log.d("JSON PARSE", e.getMessage());
            }
            return output;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            loading.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(go_context);
            builder.setMessage(s)
                    .setCancelable(false)
                    .setNeutralButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            GetBallotData ballotTask = new GetBallotData();
                            ballotTask.setMode(false);
                            ballotTask.execute("");
                        }
                    });
            msgDiag = builder.create();
            msgDiag.show();
        }
    }
}