package com.example.jethro.psitselection.Http;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jethro.psitselection.Activity.activity_01;
import com.example.jethro.psitselection.Activity.activity_02;
import com.example.jethro.psitselection.Entity.Candidate;
import com.example.jethro.psitselection.Entity.GlobalData;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jet on 10/08/2017.
 */

public class GetImage extends AsyncTask<String, Integer, ArrayList<Bitmap>> {

    private int loadSize = 0;
    private boolean mode = true;
    public void setMode(boolean bool){
        mode = bool;
    }

    @Override
    protected ArrayList<Bitmap> doInBackground(String... s) {
        ArrayList<Bitmap> bitmapList = new ArrayList<>();
        try {
            ArrayList<String> tempList = GlobalData.getCandKeyList();
            loadSize = tempList.size();
            for(int x = 0; x < loadSize; x++) {
                publishProgress(x+1);
                URL url = new URL(GlobalData.IP + "election/img/"+tempList.get(x)+".jpg");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                Bitmap img = BitmapFactory.decodeStream(conn.getInputStream());
                Log.d("BITMAP-SIZE (X,Y)", Integer.toString(img.getWidth()) + "," + Integer.toString(img.getHeight()));
                bitmapList.add(img);
                Log.d("img_load", Integer.toString(x));
            }
        } catch (Exception e) {
            Log.d("IMG!-BG", e.getMessage());
            Log.d("IMG!-BG", e.toString());
        }
        return bitmapList;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(mode){
            activity_01.loading.setMessage("Loading image "+ values+ "/" + loadSize);
        } else{
            // for act 3
            activity_02.loading.setMessage("Loading image "+ values+ "/" + loadSize);
        }
    }

    @Override
    protected void onPostExecute(ArrayList<Bitmap> bitmapList) {
        super.onPostExecute(bitmapList);
        ArrayList<Candidate> tempList = GlobalData.getCandList();
        for(int x = 0; x < loadSize; tempList.get(x).setImg(bitmapList.get(x)), x++);
        GlobalData.setCandList(tempList);

        if(mode){
            activity_01.loading.dismiss();
        } else{
            // for act 3
            activity_02.loading.dismiss();
            activity_02.proceedTOResult();
        }
    }
}
