package com.example.jethro.psitselection.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.jethro.psitselection.Entity.Candidate;
import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.Entity.Party;
import com.example.jethro.psitselection.Entity.Rank;
import com.example.jethro.psitselection.Entity.Student;
import com.example.jethro.psitselection.Http.GetBallotData;
import com.example.jethro.psitselection.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class activity_01 extends Activity implements View.OnClickListener{

    private static Context go_Context;

    private EditText et_id, et_pass;
    private Button btn_login, btn_setting;
    public static ProgressDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_01);
        go_Context = this;
        loading = new ProgressDialog(this);

        et_id = (EditText) this.findViewById(R.id.et_id);
        et_pass = (EditText) this.findViewById(R.id.et_pass);

        btn_login = (Button) this.findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        btn_setting = (Button) this.findViewById(R.id.btn_setting);
        btn_setting.setOnClickListener(this);

        if(GlobalData.isEmpty()){
            new GetBallotData().execute("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetBallotData().execute("");
        loading.setMessage("Loading");
        loading.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            // Proceed to Vote
            case R.id.btn_login:
                GlobalData.ID = et_id.getText().toString();
                String tempPass = et_pass.getText().toString() + " ";
                GlobalData.PASS = tempPass.toUpperCase();
                loading.setCancelable(false);
                loading.show();
                loading.setMessage("Loading");
                new GetStud().execute();
                break;
            //Configure IP Address
            case R.id.btn_setting:
                AlertDialog.Builder lo_builder = new AlertDialog.Builder(this);
                final EditText et_ipaddr = new EditText(this);
                et_ipaddr.setText(GlobalData.IP);
                et_ipaddr.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                ));

                lo_builder.setView(et_ipaddr);
                lo_builder.setTitle("Change Server IP");
                lo_builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        GlobalData.IP = et_ipaddr.getText().toString();
                    }
                });
                lo_builder.setNegativeButton("Cancel", null);

                Dialog ipDialog = lo_builder.create();
                ipDialog.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //code here
    }

    public void proceed(){
        Intent intent = new Intent(go_Context, activity_02.class);
        startActivityForResult(intent, 1);
    }

    public static Context getContext(){
        return go_Context;
    }

    private class GetStud extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setMessage("Logging in...");
            loading.show();
        }

        @Override
        protected String doInBackground(String... params) {
            // Download JSON via HTTP Connection
            String JSONString = "";
            try {
                URL url = new URL(GlobalData.IP + "election/student.php?id="+GlobalData.ID);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                JSONString = br.readLine();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return JSONString;
        }

        @Override
        protected void onPostExecute(String s){
            loading.dismiss();
            try{
                AlertDialog.Builder builder = new AlertDialog.Builder(go_Context);
                Dialog errorDialog;
                if(s.equals("not found")){
                    builder.setMessage("Error Log In, Invalid ID");
                    builder.setNeutralButton("Retry", null);
                    errorDialog = builder.create();
                    errorDialog.show();
                }
                else {
                    JSONObject obj = new JSONObject(s);
                    Student stud = new Student();
                    stud.setId(obj.getString("ID"));
                    stud.setFname(obj.getString("fname"));
                    stud.setLname(obj.getString("lname"));

                    int temptiny = obj.getInt("isVoted");
                    stud.setVoted(temptiny == 1);

                    GlobalData.student = stud;

                    if (GlobalData.PASS.equals(stud.getLname()) && !stud.isVoted()) {
                        builder.setMessage("Welecome " + stud.getFname() + "!");
                        builder.setNeutralButton("Proceed", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                proceed();
                            }
                        });
                        errorDialog = builder.create();
                        errorDialog.show();
                    } else {
                        builder.setMessage("Error Login, " + (stud.isVoted() ? "You already voted. " : "") + (!GlobalData.PASS.equals(stud.getLname()) ? "Wrong Password. " : ""));
                        builder.setNeutralButton("Close", null);
                        errorDialog = builder.create();
                        errorDialog.show();
                        Log.d("LOGIN LOC PASS",GlobalData.PASS + GlobalData.PASS.length());
                        Log.d("LOGIN REM PASS",stud.getLname() + stud.getLname().length());
                    }
                }
            } catch (Exception e){
                Log.d("JSON-Stud",e.getMessage());
            }
        }
    }
}
