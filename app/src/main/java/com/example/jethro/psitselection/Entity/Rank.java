package com.example.jethro.psitselection.Entity;

/**
 * Created by jksem on 10/05/2017.
 */

public class Rank {
    private String rank_id = "";
    private String rank_name = "";

    public Rank(String rank_id, String rank_name){
        this.rank_id = rank_id;
        this.rank_name = rank_name;
    }

    public String getRankId() {
        return rank_id;
    }

    public String getRankName() {
        return rank_name;
    }
}
