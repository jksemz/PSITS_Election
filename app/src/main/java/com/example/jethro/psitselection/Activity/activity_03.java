package com.example.jethro.psitselection.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import com.example.jethro.psitselection.Adapter.ResultAdapter;
import com.example.jethro.psitselection.Entity.GlobalData;
import com.example.jethro.psitselection.R;

public class activity_03 extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_03);

        ExpandableListView el_result = (ExpandableListView) this.findViewById(R.id.el_result);
        ResultAdapter adapter = new ResultAdapter(this, GlobalData.getCandList(), GlobalData.getRankList(), GlobalData.getPartyList());
        el_result.setAdapter(adapter);
    }
}
